<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\CompanyController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => 'v1'], function() {
    Route::post('login', 'AuthController@login');
    Route::post('auth/refresh', [AuthController::class, 'refreshToken']);
    Route::post('customer/login', [AuthController::class, 'loginCustomer']);
    Route::post('customer/register', [AuthController::class, 'registerCustomer']);
    Route::post('news/attachment', 'NewsController@storeAttachment');
    Route::get('news/{id}', 'NewsController@readNews');
    Route::get('news/category/{slug}', 'NewsController@categoryNews');
    Route::get('category/slug/{slug}', [CategoryController::class, 'categorySlug']);
    Route::get('news/content/publish', 'NewsController@publishNews');
    Route::get('news/content/latest-news', 'NewsController@latestNews');
    Route::get('news/content/top-news', 'NewsController@topNews');
    Route::get('news/content/popular-news', 'NewsController@popularNews');
    Route::get('category/menu', 'CategoryController@categoryMenu');
    Route::get('tags', 'TagsController@getAllTags');
    Route::get('company', [CompanyController::class, 'company']);

    Route::get('/', function() {
        return response()->json([
            'message' => 'Welcome to our API',
            'status' => 'Connected',
            'version' => env('APP_NAME', 'Laravel') . ' v1'
        ]);
    });

    Route::get('/db/connection', function() {
        try {
            $conn = DB::connection()->getPdo();
            return response()->json([
                'message' => 'Connected to database',
                'status' => 'Connected',
                'value' => DB::connection()->getDatabaseName(),
                'version' => env('APP_NAME', 'Laravel') . ' v1'
            ]);
        } catch (\Exception $e) {
            abort(500, "Connection database is Error. Please check error");
        }
    });

    Route::group(['middleware' => 'auth:sanctum'], function() {
        Route::group(['prefix' => 'tags'], function() {
            Route::post('/', 'TagsController@storeTags');
            Route::get('/{id}', 'TagsController@getTagById');
            Route::put('/{id}', 'TagsController@updateTags');
            Route::delete('/{id}', 'TagsController@deleteTags');
        });

        Route::group(['prefix' => 'category'], function() {
            Route::get('/', 'CategoryController@getAllCategories');
            Route::get('/options', 'CategoryController@categoryOptions');
            Route::post('/', 'CategoryController@storeCategories');
            Route::put('/{id}', 'CategoryController@updateCategories');
            Route::delete('/{id}', 'CategoryController@deleteCategories');
        });

        Route::group(['prefix' => 'news'], function() {
            Route::get('/', 'NewsController@getNews');
            Route::post('/', 'NewsController@storeNews');
            Route::put('/{id}', 'NewsController@updateNews');
            Route::delete('/{id}', 'NewsController@deleteNews');
        });

        Route::get('/user', 'AuthController@me');
    });
});
