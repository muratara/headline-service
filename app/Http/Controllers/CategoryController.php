<?php

namespace App\Http\Controllers;

use App\Http\Requests\CategoryRequest;
use App\Interfaces\CategoryInterface;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    protected CategoryInterface $category;

    protected Request $request;

    public function __construct(CategoryInterface $category, Request $request)
    {
        $this->category = $category;
        $this->request = $request;
    }

    public function getAllCategories()
    {
        $resp = $this->category->getAllCategories();
        return $this->callback_response($resp->status, $resp->code, $resp->message, $resp->data);
    }

    public function storeCategories(CategoryRequest $request)
    {
        $resp = $this->category->storeCategories($request);
        return $this->callback_response($resp->status, $resp->code, $resp->message, $resp->data);
    }

    public function updateCategories(CategoryRequest $request, int $id)
    {
        $resp = $this->category->updateCategories($request, $id);
        return $this->callback_response($resp->status, $resp->code, $resp->message, $resp->data);
    }

    public function deleteCategories(int $id)
    {
        $resp = $this->category->deleteCategories($id);
        return $this->callback_response($resp->status, $resp->code, $resp->message, $resp->data);
    }

    public function categoryOptions()
    {
        $resp = $this->category->categoryOptions();
        return $this->callback_response($resp->status, $resp->code, $resp->message, $resp->data);
    }

    public function categoryMenu()
    {
        $resp = $this->category->categoryMenu();
        return $this->callback_response($resp->status, $resp->code, $resp->message, $resp->data);
    }

    public function categorySlug($slug)
    {
        $resp = $this->category->categoryDetail($slug);
        return $this->callback_response($resp->status, $resp->code, $resp->message, $resp->data);
    }
}
