<?php

namespace App\Http\Controllers;

use App\Interfaces\CompanyInterface;
use Illuminate\Http\Request;

class CompanyController extends Controller
{
    protected CompanyInterface $companyRepo;

    protected Request $request;

    public function __construct(Request $request, CompanyInterface $company)
    {
        $this->companyRepo = $company;
        $this->request = $request;
    }

    public function company()
    {
        $resp = $this->companyRepo->company();
        return $this->callback_response($resp->status, $resp->code, $resp->message, $resp->data);
    }
}
