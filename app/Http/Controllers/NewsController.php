<?php

namespace App\Http\Controllers;

use App\Http\Requests\FilerRequest;
use App\Http\Requests\NewsRequest;
use App\Interfaces\FilerInterface;
use App\Interfaces\NewsInterface;
use Illuminate\Http\Request;

class NewsController extends Controller
{
    protected FilerInterface $filer;

    protected NewsInterface $news;

    protected Request $request;

    public function __construct(FilerInterface $filer, Request $request, NewsInterface $news)
    {
        $this->filer = $filer;
        $this->request = $request;
        $this->news = $news;
    }

    public function storeAttachment(FilerRequest $request)
    {
        $resp = $this->filer->uploadFile($request);
        return $this->callback_response($resp->status, $resp->code, $resp->message, $resp->data);
    }

    public function storeNews(NewsRequest $request)
    {
        $resp = $this->news->storeNews($request);
        return $this->callback_response($resp->status, $resp->code, $resp->message, $resp->data);
    }

    public function getNews()
    {
        $resp = $this->news->getNews();
        return $this->callback_response($resp->status, $resp->code, $resp->message, $resp->data);
    }

    public function updateNews(int $id, NewsRequest $request)
    {
        $resp = $this->news->updateNews($id, $request);
        return $this->callback_response($resp->status, $resp->code, $resp->message, $resp->data);
    }

    public function deleteNews(int $id)
    {
        $resp = $this->news->deleteNews($id);
        return $this->callback_response($resp->status, $resp->code, $resp->message, $resp->data);
    }

    public function readNews(int $id)
    {
        $resp = $this->news->readNews($id);
        return $this->callback_response($resp->status, $resp->code, $resp->message, $resp->data);
    }

    public function publishNews()
    {
        $resp = $this->news->getPublishNews();
        return $this->callback_response($resp->status, $resp->code, $resp->message, $resp->data);
    }

    public function latestNews()
    {
        $resp = $this->news->latestNews();
        return $this->callback_response($resp->status, $resp->code, $resp->message, $resp->data);
    }

    public function topNews()
    {
        $resp = $this->news->topNews();
        return $this->callback_response($resp->status, $resp->code, $resp->message, $resp->data);
    }

    public function popularNews()
    {
        $resp = $this->news->popularNews();
        return $this->callback_response($resp->status, $resp->code, $resp->message, $resp->data);
    }

    public function categoryNews($slug)
    {
        $resp = $this->news->getNewsByCategory($slug);
        return $this->callback_response($resp->status, $resp->code, $resp->message, $resp->data);
    }
}
