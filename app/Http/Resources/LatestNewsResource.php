<?php

namespace App\Http\Resources;

use App\Helpers\Time;
use Illuminate\Http\Resources\Json\JsonResource;

class LatestNewsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            "content" => $this->splitContentSentence($this->content) . ".",
            'image' => $this->image,
            'total_views' => $this->total_views,
            'status' => $this->status,
            'updated_by' => $this->updatedBy->name ?? $this->user->name ?? '',
            'last_update' => Time::time_elapsed_string($this->created_at),
        ];
    }

    public function splitContentSentence($content)
    {
        return preg_split('/[.!?]+/', $content, -1, PREG_SPLIT_NO_EMPTY)[0];
    }
}
