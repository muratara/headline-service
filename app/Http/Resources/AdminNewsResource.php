<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class AdminNewsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'category_id' => $this->category_id,
            'category_name' => $this->category->name ?? '',
            'image' => $this->image,
            'total_views' => $this->total_views,
            'status' => $this->status,
            'tags' => $this->tags->map(function($item) {
                return [
                    'tag_id' => $item->tag->id,
                    'tag_name' => $item->tag->name,
                ];
            }) ?? [],
            'updated_by' => $this->updatedBy->name ?? $this->user->name ?? '',
            'last_update' => date('Y-m-d H:i:s', strtotime($this->updated_at)),
        ];
    }
}
