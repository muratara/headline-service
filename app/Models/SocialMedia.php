<?php

namespace App\Models;

class SocialMedia extends \Illuminate\Database\Eloquent\Model
{
    protected $guarded = [];
    protected $table = 'social_medias';
}
