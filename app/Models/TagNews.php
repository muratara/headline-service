<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TagNews extends Model
{
    protected $table = 'tag_news';
    protected $guarded = [];

    public function tag()
    {
        return $this->belongsTo(Tag::class, 'tag_id');
    }
}
