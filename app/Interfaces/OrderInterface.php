<?php

namespace App\Interfaces;

interface OrderInterface
{
    public function createOrder();
}
