<?php
namespace App\Interfaces;

use App\Http\Requests\NewsRequest;

interface NewsInterface
{
    public function getNews(): object;
    public function storeNews(NewsRequest $request): object;
    public function updateNews(int $id, NewsRequest $request): object;
    public function deleteNews(int $id): object;
    public function readNews(int $id): object;
    public function getPublishNews(): object;
    public function latestNews(): object;
    public function topNews(): object;
    public function popularNews(): object;
    public function getNewsByCategory($categorySlug): object;
}
