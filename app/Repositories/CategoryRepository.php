<?php
namespace App\Repositories;

use App\Helpers\Text;
use App\Http\Requests\CategoryRequest;
use App\Http\Resources\CategoryMenuResource;
use App\Http\Resources\CategoryOptionResource;
use App\Http\Resources\CategoryResource;
use App\Interfaces\CategoryInterface;
use App\Models\Category;
use App\Models\GroupCategory;
use Illuminate\Support\Facades\Auth;

class CategoryRepository extends RepositoryController implements CategoryInterface
{
    public function getAllCategories(): object
    {
        try {
            $categories = Category::orderBy('updated_at', 'desc')->paginate($this->getLimitPage());
            CategoryResource::collection($categories);
            return $this->callback_response("success", 200, 'Get all categories success', $categories);
        } catch (\Exception $th) {
            report($th);
            abort(500, $th->getMessage());
        }
    }

    public function storeCategories(CategoryRequest $request): object
    {
        try {
            if ($this->duplicateEntry()) return $this->callback_response("duplicate_entry", 400, 'Category is exist');

            $body = $request->all();
            $body['slug'] = Text::slugify($body['name']);
            $body['created_by'] = Auth::user()->id;
            $category = Category::create($body);
            return $this->callback_response("success", 200, 'Create category success', [
                'category' => new CategoryResource($category)
            ]);
        } catch (\Exception $th) {
            report($th);
            abort(500, $th->getMessage());
        }
    }

    public function updateCategories(CategoryRequest $request, int $id): object
    {
        try {
            $category = Category::findOrFail($id);
            if ($this->duplicateEntry()) return $this->callback_response("duplicate_entry", 400, 'Category is exist');

            $body = $request->all();
            $body['slug'] = Text::slugify($body['name']);
            $body['updated_by'] = Auth::user()->id;
            $category->update($body);
            return $this->callback_response("success", 200, 'Update category success', [
                'category' => new CategoryResource($category)
            ]);
        } catch (\Exception $th) {
            report($th);
            abort(500, $th->getMessage());
        }
    }

    public function deleteCategories(int $id): object
    {
        try {
            $category = Category::findOrFail($id);
            $category->delete();
            return $this->callback_response("success", 200, 'Delete category success', [
                'category' => new CategoryResource($category)
            ]);
        } catch (\Exception $th) {
            report($th);
            abort(500, $th->getMessage());
        }
    }

    protected function duplicateEntry()
    {
        $tag = Category::where('name', request()->name)->first();
        if ($tag) return true;
        return false;
    }

    public function categoryOptions(): object
    {
        $categories = Category::all();
        return $this->callback_response("success", 200, 'Get all categories success', CategoryOptionResource::collection($categories));
    }

    public function categoryMenu(): object
    {
        $groupCategory = GroupCategory::orderBy('id', 'asc')->get();
        if (request()->has('is_menu')) {
            foreach ($groupCategory as $value) {
                $categories = Category::orderBy('id', 'desc')->where('group_category', $value->id);
                if (request()->is_menu == 'true') {
                    $categories = $categories->where('is_show_menu', true);
                }

                if (request()->is_menu == 'false') {
                    $categories = $categories->where('is_show_menu', false);
                }

                $value->sub_category = CategoryMenuResource::collection($categories->get());
            }
        }
        return $this->callback_response("success", 200, 'Get all categories success', CategoryMenuResource::collection($groupCategory));
    }

    public function categoryDetail($slug): object
    {
        $category = GroupCategory::where('slug', $slug)->first();
        if (!$category) {
            $category = Category::where('slug', $slug)->first();
        }
        return $this->callback_response("success", 200, 'Get categories success', $category);
    }
}
